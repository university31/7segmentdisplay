/**
 * 
 */
package org.seven.segment;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.io.Serializable;

/**
 * @author Seba i Mati
 *
 */
public class SevenSegment extends Component implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 *	private variables 
	 */
	// private variables
	private boolean a;
	private boolean b;
	private boolean c;
	private boolean d;
	private boolean e;
	private boolean f;
	private boolean g;
	private boolean dot;
	private boolean flash;
	private String value;
	// colors
	private Color on;
	private Color off;
	private static final Color RED = new Color(255, 0, 0);
	private static final Color DARK_RED = new Color(78, 0, 0);
	private static final Color GREEN = new Color(0, 255, 0);
	private static final Color DARK_GREEN = new Color(0, 78, 0);
	private static final Color BLUE = new Color(0, 0, 255);
	private static final Color DARK_BLUE = new Color(0, 0, 78);
	// segments
	private Polygon polygonA;
	private Polygon polygonB;
	private Polygon polygonC;
	private Polygon polygonD;
	private Polygon polygonE;
	private Polygon polygonF;
	private Polygon polygonG;
	private Polygon polygonDot;
	
	
	
	public SevenSegment() {
		setOn(RED);
		setOff(DARK_RED);
		setForeground(off);
		setBackground(Color.BLACK);
	}
	public synchronized void paint(Graphics g)
	{
		Graphics2D g2d = (Graphics2D)g;
		//A
		polygonA = new Polygon();
		polygonA.addPoint(10, 10);
		polygonA.addPoint(20, 0);
		polygonA.addPoint(60, 0);
		polygonA.addPoint(70, 10);
		polygonA.addPoint(60, 20);
		polygonA.addPoint(20, 20);
		//draw polygon
		g2d.drawPolygon(polygonA);
		g2d.fillPolygon(polygonA);
		//B
		polygonB = new Polygon();
		polygonB.addPoint(70, 12);
		polygonB.addPoint(80, 22);
		polygonB.addPoint(80, 62);
		polygonB.addPoint(70, 72);
		polygonB.addPoint(60, 62);
		polygonB.addPoint(60, 22);
		//draw polygon
		g2d.drawPolygon(polygonB);
		g2d.fillPolygon(polygonB);
		//C
		polygonC = new Polygon();
		polygonC.addPoint(70, 76);
		polygonC.addPoint(80, 86);
		polygonC.addPoint(80, 126);
		polygonC.addPoint(70, 136);
		polygonC.addPoint(60, 126);
		polygonC.addPoint(60, 86);
		//draw polygon
		g2d.drawPolygon(polygonC);
		g2d.fillPolygon(polygonC);
		//D
		polygonD = new Polygon();
		polygonD.addPoint(70, 138);
		polygonD.addPoint(60, 148);
		polygonD.addPoint(20, 148);
		polygonD.addPoint(10, 138);
		polygonD.addPoint(20, 128);
		polygonD.addPoint(60, 128);
		//draw polygon
		g2d.drawPolygon(polygonD);
		g2d.fillPolygon(polygonD);
		//E
		polygonE = new Polygon();
		polygonE.addPoint(10, 76);
		polygonE.addPoint(20, 86);
		polygonE.addPoint(20, 126);
		polygonE.addPoint(10, 136);
		polygonE.addPoint(0, 126);
		polygonE.addPoint(0, 86);
		//draw polygon
		g2d.drawPolygon(polygonE);
		g2d.fillPolygon(polygonE);
		//F
		polygonF = new Polygon();
		polygonF.addPoint(10, 12);
		polygonF.addPoint(20, 22);
		polygonF.addPoint(20, 62);
		polygonF.addPoint(10, 72);
		polygonF.addPoint(0, 62);
		polygonF.addPoint(0, 22);
		//draw polygon
		g2d.drawPolygon(polygonF);
		g2d.fillPolygon(polygonF);
		//G
		polygonG = new Polygon();
		polygonG.addPoint(70, 74);
		polygonG.addPoint(60, 84);
		polygonG.addPoint(20, 84);
		polygonG.addPoint(10, 74);
		polygonG.addPoint(20, 64);
		polygonG.addPoint(60, 64);
		//draw polygon
		g2d.drawPolygon(polygonG);
		g2d.fillPolygon(polygonG);
		//dot
		polygonDot = new Polygon();
		polygonDot.addPoint(85, 128);
		polygonDot.addPoint(105, 128);
		polygonDot.addPoint(105, 148);
		polygonDot.addPoint(85, 148);
		//draw polygon
		g2d.drawPolygon(polygonDot);
		g2d.fillPolygon(polygonDot);
		
	}
	public void setSegments(String value)
	{
		setA(false);
		setB(false);
		setC(false);
		setD(false);
		setE(false);
		setF(false);
		setG(false);
		setDot(false);	
		switch(value) {
		  case ".":
			  setDot(true);
			  break;
		  case "0":
			  //only g off
			  setA(true);
			  setB(true);
			  setC(true);
			  setD(true);
			  setE(true);
			  setF(true);
			  break;
		  case "1":
			  //b and c on
			  setB(true);
			  setC(true);
			  break;
		  case "2":
			  //c and f off
			  setA(true);
			  setB(true);
			  setD(true);
			  setE(true);
			  setG(true);
			  break;
		  case "3":
			  //f and e off
			  setA(true);
			  setB(true);
			  setC(true);
			  setD(true);
			  setG(true);
			  break;
		  case "4":
			  //a, d and e off
			  setB(true);
			  setC(true);
			  setF(true);
			  setG(true);
			  break;
		  case "5":
			  //b and e off
			  setA(true);
			  setC(true);
			  setD(true);
			  setF(true);
			  setG(true);
			  break;
		  case "6":
			  //b is only off
			  setA(true);
			  setC(true);
			  setD(true);
			  setE(true);
			  setF(true);
			  setG(true);
			    break;
		  case "7":
			  //a, b, c on
			  setA(true);
			  setB(true);
			  setC(true);
			  break;
		  case "8":
			  //all on
			  setA(true);
			  setB(true);
			  setC(true);
			  setD(true);
			  setE(true);
			  setF(true);
			  setG(true);
			  break;
		  case "9":
			  //only e is off
			  setA(true);
			  setB(true);
			  setC(true);
			  setD(true);
			  setF(true);
			  setG(true);
			  break;
		  case "A":
			  //only d off
			  setA(true);
			  setB(true);
			  setC(true);
			  setE(true);
			  setF(true);
			  setG(true);
			  break;
		  case "B":
			  //a and b is off
			  setC(true);
			  setD(true);
			  setE(true);
			  setF(true);
			  setG(true);
			  break;
		  case "C":
			  //b, c, g off
			  setA(true);
			  setD(true);
			  setE(true);
			  setF(true);
			  break;
		  case "D":
			  //a and f off
			  setB(true);
			  setC(true);
			  setD(true);
			  setE(true);
			  setG(true);
			  break;
		  case "E":
			  //just b and c off
			  setA(true);
			  setD(true);
			  setE(true);
			  setF(true);
			  setG(true);
			  break;
		  case "F":
			  //b, c and d off
			  setA(true);
			  setE(true);
			  setF(true);
			  setG(true);
		      break;
		  default:
		      //wrong value, don`t set anything
			  System.out.println("You set wrong value");
		}
	}
	public void lightUp(Graphics g)
	{
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(on);
		if(this.a == true)
		{
			g2d.fillPolygon(polygonA);
		}
		if(this.b == true)
		{
			g2d.fillPolygon(polygonB);
		}
		if(this.c == true)
		{
			g2d.fillPolygon(polygonC);
		}
		if(this.d == true)
		{
			g2d.fillPolygon(polygonD);
		}
		if(this.e == true)
		{
			g2d.fillPolygon(polygonE);
		}
		if(this.f == true)
		{
			g2d.fillPolygon(polygonF);
		}
		if(this.g == true)
		{
			g2d.fillPolygon(polygonG);
		}
		if(this.dot == true)
		{
			g2d.fillPolygon(polygonDot);
		}
	}
	public void clearSegments(Graphics g) {
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(off);
			g2d.fillPolygon(polygonA);
			g2d.fillPolygon(polygonB);
			g2d.fillPolygon(polygonC);
			g2d.fillPolygon(polygonD);
			g2d.fillPolygon(polygonE);
			g2d.fillPolygon(polygonF);
			g2d.fillPolygon(polygonG);
			g2d.fillPolygon(polygonDot);		
	}
	public void flashing()
	{
		this.lightUp(this.getGraphics());
		try {
			Thread.sleep(500);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		this.clearSegments(this.getGraphics());
		try {
			Thread.sleep(500);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	public void changeFlash()
	{
		flash = !(flash);
	}
	public void changeSegmentsColor(int type)
	{
		if(type == 1)
		{
			setOn(RED);
			setOff(DARK_RED);
			clearSegments(this.getGraphics());
		}
		else if(type == 2)
		{
			setOn(GREEN);
			setOff(DARK_GREEN);
			clearSegments(this.getGraphics());
		}
		else if(type == 3)
		{
			setOn(BLUE);
			setOff(DARK_BLUE);
			clearSegments(this.getGraphics());
		}
		else
		{
			System.out.print("Something goes wrong");
		}
	}
	
	/**
	 * @param a
	 * @param b
	 * @param c
	 * @param d
	 * @param e
	 * @param f
	 * @param g
	 * @param dot
	 * @param value
	 */
	public SevenSegment(boolean a, boolean b, boolean c, boolean d, boolean e, boolean f, boolean g, boolean dot,
			String value) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.e = e;
		this.f = f;
		this.g = g;
		this.dot = dot;
		this.value = value;
	}



	/**
	 * @return the a
	 */
	public boolean isA() {
		return a;
	}



	/**
	 * @param a the a to set
	 */
	public void setA(boolean a) {
		this.a = a;
	}



	/**
	 * @return the b
	 */
	public boolean isB() {
		return b;
	}



	/**
	 * @param b the b to set
	 */
	public void setB(boolean b) {
		this.b = b;
	}



	/**
	 * @return the c
	 */
	public boolean isC() {
		return c;
	}



	/**
	 * @param c the c to set
	 */
	public void setC(boolean c) {
		this.c = c;
	}



	/**
	 * @return the d
	 */
	public boolean isD() {
		return d;
	}



	/**
	 * @param d the d to set
	 */
	public void setD(boolean d) {
		this.d = d;
	}



	/**
	 * @return the e
	 */
	public boolean isE() {
		return e;
	}



	/**
	 * @param e the e to set
	 */
	public void setE(boolean e) {
		this.e = e;
	}



	/**
	 * @return the f
	 */
	public boolean isF() {
		return f;
	}



	/**
	 * @param f the f to set
	 */
	public void setF(boolean f) {
		this.f = f;
	}



	/**
	 * @return the g
	 */
	public boolean isG() {
		return g;
	}



	/**
	 * @param g the g to set
	 */
	public void setG(boolean g) {
		this.g = g;
	}



	/**
	 * @return the dot
	 */
	public boolean isDot() {
		return dot;
	}



	/**
	 * @param dot the dot to set
	 */
	public void setDot(boolean dot) {
		this.dot = dot;
	}



	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}



	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (a ? 1231 : 1237);
		result = prime * result + (b ? 1231 : 1237);
		result = prime * result + (c ? 1231 : 1237);
		result = prime * result + (d ? 1231 : 1237);
		result = prime * result + (dot ? 1231 : 1237);
		result = prime * result + (e ? 1231 : 1237);
		result = prime * result + (f ? 1231 : 1237);
		result = prime * result + (g ? 1231 : 1237);
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SevenSegment other = (SevenSegment) obj;
		if (a != other.a)
			return false;
		if (b != other.b)
			return false;
		if (c != other.c)
			return false;
		if (d != other.d)
			return false;
		if (dot != other.dot)
			return false;
		if (e != other.e)
			return false;
		if (f != other.f)
			return false;
		if (g != other.g)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	/**
	 * @return the flash
	 */
	public boolean isFlash() {
		return flash;
	}
	/**
	 * @param flash the flash to set
	 */
	public void setFlash(boolean flash) {
		this.flash = flash;
	}
	/**
	 * @param flash the flash to get
	 */
	public boolean getFlash() {
		return this.flash;
	}
	/**
	 * @return the on
	 */
	public Color getOn() {
		return on;
	}
	/**
	 * @param on the on to set
	 */
	public void setOn(Color on) {
		this.on = on;
	}
	/**
	 * @return the off
	 */
	public Color getOff() {
		return off;
	}
	/**
	 * @param off the off to set
	 */
	public void setOff(Color off) {
		this.off = off;
	}

}
