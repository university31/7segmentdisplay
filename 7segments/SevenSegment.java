/**
 * Documentation for JavaBeans component "Seven Segments Display".
 */
package org.seven.segment;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.io.Serializable;

/**
 * @author Sebastian Kalinowski
 * @author Mateusz Dyrda
 *
 */
public class SevenSegment extends Component implements Serializable {

	
	private static final long serialVersionUID = 1L;
	/**
	 *	private variables which methods are using.
	 */
	// private variables
	private boolean a;
	private boolean b;
	private boolean c;
	private boolean d;
	private boolean e;
	private boolean f;
	private boolean g;
	private boolean dot;
	private boolean flash;
	private String value;
	private int startX;
	private int startY;
	private int numberDisp;
	private int position;
	// colors
	private Color on;
	private Color off;
	private static final Color RED = new Color(255, 0, 0);
	private static final Color DARK_RED = new Color(78, 0, 0);
	private static final Color GREEN = new Color(0, 255, 0);
	private static final Color DARK_GREEN = new Color(0, 78, 0);
	private static final Color BLUE = new Color(0, 0, 255);
	private static final Color DARK_BLUE = new Color(0, 0, 78);
	// segments Polygon objects
	private Polygon[] polygonA;
	private Polygon[] polygonB;
	private Polygon[] polygonC;
	private Polygon[] polygonD;
	private Polygon[] polygonE;
	private Polygon[] polygonF;
	private Polygon[] polygonG;
	private Polygon[] polygonDot;
	
	
	/**
	 * Constructor without parameters where the basics variables are initialized.
	 */
	public SevenSegment() {
		//setting basics parameters
		setOn(RED);
		setOff(DARK_RED);
		setForeground(off);
		setBackground(Color.BLACK);
		//initialization of Polygon 
		polygonA = new Polygon[5];
		polygonB = new Polygon[5];
		polygonC = new Polygon[5];
		polygonD = new Polygon[5];
		polygonE = new Polygon[5];
		polygonF = new Polygon[5];
		polygonG = new Polygon[5];
		polygonDot = new Polygon[5];
		//initialization of variables
		startX = 0;
		startY = 0;
		numberDisp = 0;
		position = 0;
	}
	/**
	 * Synchronized method which is called when constructor is creating.
	 * Method paint a single seven segment display.
	 * 
	 * @param	g	an Graphics class that allow an application to draw
	 * 
	 * @see	Graphics2D
	 */
	public synchronized void paint(Graphics g)
	{
		Graphics2D g2d = (Graphics2D)g;
		//background
		g2d.setColor(Color.BLACK);
		g2d.drawRect(startX, startY, 110, 150);
		g2d.fillRect(startX, startY, 110, 150);
		g2d.setColor(off);
		//A
		polygonA[0] = new Polygon();
		polygonA[0].addPoint(10, 10);
		polygonA[0].addPoint(20, 0);
		polygonA[0].addPoint(60, 0);
		polygonA[0].addPoint(70, 10);
		polygonA[0].addPoint(60, 20);
		polygonA[0].addPoint(20, 20);
		//draw polygon
		g2d.drawPolygon(polygonA[0]);
		g2d.fillPolygon(polygonA[0]);
		//B
		polygonB[0] = new Polygon();
		polygonB[0].addPoint(70, 12);
		polygonB[0].addPoint(80, 22);
		polygonB[0].addPoint(80, 62);
		polygonB[0].addPoint(70, 72);
		polygonB[0].addPoint(60, 62);
		polygonB[0].addPoint(60, 22);
		//draw polygon
		g2d.drawPolygon(polygonB[0]);
		g2d.fillPolygon(polygonB[0]);
		//C
		polygonC[0] = new Polygon();
		polygonC[0].addPoint(70, 76);
		polygonC[0].addPoint(80, 86);
		polygonC[0].addPoint(80, 126);
		polygonC[0].addPoint(70, 136);
		polygonC[0].addPoint(60, 126);
		polygonC[0].addPoint(60, 86);
		//draw polygon
		g2d.drawPolygon(polygonC[0]);
		g2d.fillPolygon(polygonC[0]);
		//D
		polygonD[0] = new Polygon();
		polygonD[0].addPoint(70, 138);
		polygonD[0].addPoint(60, 148);
		polygonD[0].addPoint(20, 148);
		polygonD[0].addPoint(10, 138);
		polygonD[0].addPoint(20, 128);
		polygonD[0].addPoint(60, 128);
		//draw polygon
		g2d.drawPolygon(polygonD[0]);
		g2d.fillPolygon(polygonD[0]);
		//E
		polygonE[0] = new Polygon();
		polygonE[0].addPoint(10, 76);
		polygonE[0].addPoint(20, 86);
		polygonE[0].addPoint(20, 126);
		polygonE[0].addPoint(10, 136);
		polygonE[0].addPoint(0, 126);
		polygonE[0].addPoint(0, 86);
		//draw polygon
		g2d.drawPolygon(polygonE[0]);
		g2d.fillPolygon(polygonE[0]);
		//F
		polygonF[0] = new Polygon();
		polygonF[0].addPoint(10, 12);
		polygonF[0].addPoint(20, 22);
		polygonF[0].addPoint(20, 62);
		polygonF[0].addPoint(10, 72);
		polygonF[0].addPoint(0, 62);
		polygonF[0].addPoint(0, 22);
		//draw polygon
		g2d.drawPolygon(polygonF[0]);
		g2d.fillPolygon(polygonF[0]);
		//G
		polygonG[0] = new Polygon();
		polygonG[0].addPoint(70, 74);
		polygonG[0].addPoint(60, 84);
		polygonG[0].addPoint(20, 84);
		polygonG[0].addPoint(10, 74);
		polygonG[0].addPoint(20, 64);
		polygonG[0].addPoint(60, 64);
		//draw polygon
		g2d.drawPolygon(polygonG[0]);
		g2d.fillPolygon(polygonG[0]);
		//dot
		polygonDot[0] = new Polygon();
		polygonDot[0].addPoint(85, 128);
		polygonDot[0].addPoint(105, 128);
		polygonDot[0].addPoint(105, 148);
		polygonDot[0].addPoint(85, 148);
		//draw polygon
		g2d.drawPolygon(polygonDot[0]);
		g2d.fillPolygon(polygonDot[0]);
		
	}
	/**
	 *	Method that set the boolean values based on the sign which we called.
	 *
	 *@param	value	an char variable that define what to set on true and what to false
	 *
	 */
	public void setSegments(char value)
	{
		setA(false);
		setB(false);
		setC(false);
		setD(false);
		setE(false);
		setF(false);
		setG(false);
		setDot(false);	
		switch(value) {
		  case '.':
			  setDot(true);
			  break;
		  case '0':
			  //only g off
			  setA(true);
			  setB(true);
			  setC(true);
			  setD(true);
			  setE(true);
			  setF(true);
			  break;
		  case '1':
			  //b and c on
			  setB(true);
			  setC(true);
			  break;
		  case '2':
			  //c and f off
			  setA(true);
			  setB(true);
			  setD(true);
			  setE(true);
			  setG(true);
			  break;
		  case '3':
			  //f and e off
			  setA(true);
			  setB(true);
			  setC(true);
			  setD(true);
			  setG(true);
			  break;
		  case '4':
			  //a, d and e off
			  setB(true);
			  setC(true);
			  setF(true);
			  setG(true);
			  break;
		  case '5':
			  //b and e off
			  setA(true);
			  setC(true);
			  setD(true);
			  setF(true);
			  setG(true);
			  break;
		  case '6':
			  //b is only off
			  setA(true);
			  setC(true);
			  setD(true);
			  setE(true);
			  setF(true);
			  setG(true);
			    break;
		  case '7':
			  //a, b, c on
			  setA(true);
			  setB(true);
			  setC(true);
			  break;
		  case '8':
			  //all on
			  setA(true);
			  setB(true);
			  setC(true);
			  setD(true);
			  setE(true);
			  setF(true);
			  setG(true);
			  break;
		  case '9':
			  //only e is off
			  setA(true);
			  setB(true);
			  setC(true);
			  setD(true);
			  setF(true);
			  setG(true);
			  break;
		  case 'E':
			  //b and c is off
			  setA(true);
			  setD(true);
			  setE(true);
			  setF(true);
			  setG(true);
			  break;
		  case 'r':
			  //e and g is on
			  setE(true);
			  setG(true);
			  break;
		  case 'o':
			  //a, b and e is off
			  setC(true);
			  setD(true);
			  setE(true);
			  setG(true);
			  break;
		  case '-':
			  //just g is on
			  setG(true);
			  break;
		  default:
		      //wrong value, don`t set anything
			  System.out.println("You set wrong value");
		}
	}
	/**
	 *	Method that based on which segments are in true state lights up segments.
	 *
	 *@param	g	an Graphics object that allows to draw.
	 *@param	dispCount	an integer value that show which display we choose.
	 *
	 *@see	Graphics
	 */
	public void lightUp(Graphics g, int dispCount)
	{
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(on);
		if(this.a == true)
		{
			g2d.fillPolygon(polygonA[dispCount]);
		}
		if(this.b == true)
		{
			g2d.fillPolygon(polygonB[dispCount]);
		}
		if(this.c == true)
		{
			g2d.fillPolygon(polygonC[dispCount]);
		}
		if(this.d == true)
		{
			g2d.fillPolygon(polygonD[dispCount]);
		}
		if(this.e == true)
		{
			g2d.fillPolygon(polygonE[dispCount]);
		}
		if(this.f == true)
		{
			g2d.fillPolygon(polygonF[dispCount]);
		}
		if(this.g == true)
		{
			g2d.fillPolygon(polygonG[dispCount]);
		}
		if(this.dot == true)
		{
			g2d.fillPolygon(polygonDot[dispCount]);
		}
	}
	/**
	 *	Method that clear all segments and making display off.
	 *
	 *@param	g	an Graphics object that allowed to draw.
	 *
	 *@see Graphics
	 */
	public void clearSegments(Graphics g) {
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(off);
		for(int i = 0; i <= numberDisp; i++)
		{
			g2d.fillPolygon(polygonA[i]);
			g2d.fillPolygon(polygonB[i]);
			g2d.fillPolygon(polygonC[i]);
			g2d.fillPolygon(polygonD[i]);
			g2d.fillPolygon(polygonE[i]);
			g2d.fillPolygon(polygonF[i]);
			g2d.fillPolygon(polygonG[i]);
			g2d.fillPolygon(polygonDot[i]);	
		}
				
	}
	/**
	 * Method that allowed to flashing display several times. Based on value that we accept.
	 * 
	 * @param	counter		an integer value that determines how many flash we want.
	 * @param	value	an String object that contains value which is displaying.
	 */
	public void flashing(int counter, String value)
	{
		for(int i = 0; i < counter; i++)
		{
			this.setDisplayValue(value);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e1) {
				//error 
				e1.printStackTrace();
			}
			this.clearSegments(this.getGraphics());
			try {
				Thread.sleep(500);
			} catch (InterruptedException e1) {
				//error
				e1.printStackTrace();
			}
		}

		this.setDisplayValue(value);
		
	}
	/**
	 * Method that allowed to choose the segments color. Depends which type we choose we can get 
	 * 1 - red, 2 - green, 3 - blue
	 * 
	 * @param	type	an integer value which color we want.
	 */
	public void changeSegmentsColor(int type)
	{
		if(type == 1)
		{
			setOn(RED);
			setOff(DARK_RED);
			clearSegments(this.getGraphics());
		}
		else if(type == 2)
		{
			setOn(GREEN);
			setOff(DARK_GREEN);
			clearSegments(this.getGraphics());
		}
		else if(type == 3)
		{
			setOn(BLUE);
			setOff(DARK_BLUE);
			clearSegments(this.getGraphics());
		}
		else
		{
			System.out.print("Something goes wrong");
		}
	}
	/**
	 *	Method that allowed to add another display next to last in number.
	 *
	 *@param	g	an Graphics object that allowed to draw.
	 */
	public void addDisplay(Graphics g) 
	{
		Graphics2D g2d = (Graphics2D)g;
		this.clearSegments(g2d);
		g2d.setColor(on);
		numberDisp++;
		//background
				g2d.setColor(Color.BLACK);
				g2d.drawRect(startX + (numberDisp * 110), startY, 110, 150);
				g2d.fillRect(startX + (numberDisp * 110), startY, 110, 150);
				g2d.setColor(off);
				//A
				polygonA[numberDisp] = new Polygon();
				polygonA[numberDisp].addPoint(startX + (numberDisp * 110) + 10, 10);
				polygonA[numberDisp].addPoint(startX + (numberDisp * 110) + 20, 0);
				polygonA[numberDisp].addPoint(startX + (numberDisp * 110) + 60, 0);
				polygonA[numberDisp].addPoint(startX + (numberDisp * 110) + 70, 10);
				polygonA[numberDisp].addPoint(startX + (numberDisp * 110) + 60, 20);
				polygonA[numberDisp].addPoint(startX + (numberDisp * 110) + 20, 20);
				//draw polygon
				g2d.drawPolygon(polygonA[numberDisp]);
				g2d.fillPolygon(polygonA[numberDisp]);
				//B
				polygonB[numberDisp] = new Polygon();
				polygonB[numberDisp].addPoint(startX + (numberDisp * 110) + 70, 12);
				polygonB[numberDisp].addPoint(startX + (numberDisp * 110) + 80, 22);
				polygonB[numberDisp].addPoint(startX + (numberDisp * 110) + 80, 62);
				polygonB[numberDisp].addPoint(startX + (numberDisp * 110) + 70, 72);
				polygonB[numberDisp].addPoint(startX + (numberDisp * 110) + 60, 62);
				polygonB[numberDisp].addPoint(startX + (numberDisp * 110) + 60, 22);
				//draw polygon
				g2d.drawPolygon(polygonB[numberDisp]);
				g2d.fillPolygon(polygonB[numberDisp]);
				//C
				polygonC[numberDisp] = new Polygon();
				polygonC[numberDisp].addPoint(startX + (numberDisp * 110) + 70, 76);
				polygonC[numberDisp].addPoint(startX + (numberDisp * 110) + 80, 86);
				polygonC[numberDisp].addPoint(startX + (numberDisp * 110) + 80, 126);
				polygonC[numberDisp].addPoint(startX + (numberDisp * 110) + 70, 136);
				polygonC[numberDisp].addPoint(startX + (numberDisp * 110) + 60, 126);
				polygonC[numberDisp].addPoint(startX + (numberDisp * 110) + 60, 86);
				//draw polygon
				g2d.drawPolygon(polygonC[numberDisp]);
				g2d.fillPolygon(polygonC[numberDisp]);
				//D
				polygonD[numberDisp] = new Polygon();
				polygonD[numberDisp].addPoint(startX + (numberDisp * 110) + 70, 138);
				polygonD[numberDisp].addPoint(startX + (numberDisp * 110) + 60, 148);
				polygonD[numberDisp].addPoint(startX + (numberDisp * 110) + 20, 148);
				polygonD[numberDisp].addPoint(startX + (numberDisp * 110) + 10, 138);
				polygonD[numberDisp].addPoint(startX + (numberDisp * 110) + 20, 128);
				polygonD[numberDisp].addPoint(startX + (numberDisp * 110) + 60, 128);
				//draw polygon
				g2d.drawPolygon(polygonD[numberDisp]);
				g2d.fillPolygon(polygonD[numberDisp]);
				//E
				polygonE[numberDisp] = new Polygon();
				polygonE[numberDisp].addPoint(startX + (numberDisp * 110) + 10, 76);
				polygonE[numberDisp].addPoint(startX + (numberDisp * 110) + 20, 86);
				polygonE[numberDisp].addPoint(startX + (numberDisp * 110) + 20, 126);
				polygonE[numberDisp].addPoint(startX + (numberDisp * 110) + 10, 136);
				polygonE[numberDisp].addPoint(startX + (numberDisp * 110) + 0, 126);
				polygonE[numberDisp].addPoint(startX + (numberDisp * 110) + 0, 86);
				//draw polygon
				g2d.drawPolygon(polygonE[numberDisp]);
				g2d.fillPolygon(polygonE[numberDisp]);
				//F
				polygonF[numberDisp] = new Polygon();
				polygonF[numberDisp].addPoint(startX + (numberDisp * 110) + 10, 12);
				polygonF[numberDisp].addPoint(startX + (numberDisp * 110) + 20, 22);
				polygonF[numberDisp].addPoint(startX + (numberDisp * 110) + 20, 62);
				polygonF[numberDisp].addPoint(startX + (numberDisp * 110) + 10, 72);
				polygonF[numberDisp].addPoint(startX + (numberDisp * 110) + 0, 62);
				polygonF[numberDisp].addPoint(startX + (numberDisp * 110) + 0, 22);
				//draw polygon
				g2d.drawPolygon(polygonF[numberDisp]);
				g2d.fillPolygon(polygonF[numberDisp]);
				//G
				polygonG[numberDisp] = new Polygon();
				polygonG[numberDisp].addPoint(startX + (numberDisp * 110) + 70, 74);
				polygonG[numberDisp].addPoint(startX + (numberDisp * 110) + 60, 84);
				polygonG[numberDisp].addPoint(startX + (numberDisp * 110) + 20, 84);
				polygonG[numberDisp].addPoint(startX + (numberDisp * 110) + 10, 74);
				polygonG[numberDisp].addPoint(startX + (numberDisp * 110) + 20, 64);
				polygonG[numberDisp].addPoint(startX + (numberDisp * 110) + 60, 64);
				//draw polygon
				g2d.drawPolygon(polygonG[numberDisp]);
				g2d.fillPolygon(polygonG[numberDisp]);
				//dot
				polygonDot[numberDisp] = new Polygon();
				polygonDot[numberDisp].addPoint(startX + (numberDisp * 110) + 85, 128);
				polygonDot[numberDisp].addPoint(startX + (numberDisp * 110) + 105, 128);
				polygonDot[numberDisp].addPoint(startX + (numberDisp * 110) + 105, 148);
				polygonDot[numberDisp].addPoint(startX + (numberDisp * 110) + 85, 148);
				//draw polygon
				g2d.drawPolygon(polygonDot[numberDisp]);
				g2d.fillPolygon(polygonDot[numberDisp]);
	}
	/**
	 * Method that delete an last display in number.
	 * 
	 * @param	g	an Graphics object that allowed to draw.
	 */
	public void deleteDisplay(Graphics g) 
	{
		Graphics2D g2d = (Graphics2D)g;
		this.clearSegments(g2d);
		g2d.setColor(on);
		//background
				g2d.setColor(Color.WHITE);
				g2d.drawRect(startX + (numberDisp * 110), startY, 110, 150);
				g2d.fillRect(startX + (numberDisp * 110), startY, 110, 150);

		numberDisp--;
	}
	/**
	 *	Method that based on value which we accept display it on displays.
	 *
	 *@param	value	an String object where we define displaying value.
	 */
	public void setDisplayValue(String value)
	{

		this.clearSegments(this.getGraphics());
		int ile;
		if(value.contains("."))
		{
			ile = value.length() - 2;
			
		}
		else ile = value.length() - 1;

		position = value.length() - 1;
		if(position <= numberDisp)
		{
			for(int i = 0; i <= ile;i++)
			{
				char valueAt;
				valueAt = value.charAt(position - i);
				if(valueAt == '.')
				{
					valueAt = value.charAt(position - i - 1);
					this.setSegments(valueAt);
					this.setDot(true);
					this.lightUp(this.getGraphics(), numberDisp - i);
				}
				else {
					this.setSegments(valueAt);
					this.lightUp(this.getGraphics(), numberDisp - i);
				}
			}
		}			
		else this.setError();
	}
	/**
	 * Method without parameters that display "Er", "Err" or "Error" based on how much display we had added.
	 */
	public void setError()
	{
		if(this.numberDisp == 1)
		{
			this.setDisplayValue("Er");
		}
		else if((this.numberDisp >=2) && (this.numberDisp < 4))
		{
			this.setDisplayValue("Err");
			
		}
		else if(this.numberDisp == 4)
		{
			this.setDisplayValue("Error");
			
		}
		else System.out.print("ERROR W ERROR");
	}
	/**
	 * @param a
	 * @param b
	 * @param c
	 * @param d
	 * @param e
	 * @param f
	 * @param g
	 * @param dot
	 * @param value
	 */
	public SevenSegment(boolean a, boolean b, boolean c, boolean d, boolean e, boolean f, boolean g, boolean dot,
			String value) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.e = e;
		this.f = f;
		this.g = g;
		this.dot = dot;
		this.value = value;
	}

	/**
	 * Setters and getters.
	 */

	/**
	 * @return the a
	 */
	public boolean isA() {
		return a;
	}



	/**
	 * @param a the a to set
	 */
	public void setA(boolean a) {
		this.a = a;
	}



	/**
	 * @return the b
	 */
	public boolean isB() {
		return b;
	}



	/**
	 * @param b the b to set
	 */
	public void setB(boolean b) {
		this.b = b;
	}



	/**
	 * @return the c
	 */
	public boolean isC() {
		return c;
	}



	/**
	 * @param c the c to set
	 */
	public void setC(boolean c) {
		this.c = c;
	}



	/**
	 * @return the d
	 */
	public boolean isD() {
		return d;
	}



	/**
	 * @param d the d to set
	 */
	public void setD(boolean d) {
		this.d = d;
	}



	/**
	 * @return the e
	 */
	public boolean isE() {
		return e;
	}



	/**
	 * @param e the e to set
	 */
	public void setE(boolean e) {
		this.e = e;
	}



	/**
	 * @return the f
	 */
	public boolean isF() {
		return f;
	}



	/**
	 * @param f the f to set
	 */
	public void setF(boolean f) {
		this.f = f;
	}



	/**
	 * @return the g
	 */
	public boolean isG() {
		return g;
	}



	/**
	 * @param g the g to set
	 */
	public void setG(boolean g) {
		this.g = g;
	}



	/**
	 * @return the dot
	 */
	public boolean isDot() {
		return dot;
	}



	/**
	 * @param dot the dot to set
	 */
	public void setDot(boolean dot) {
		this.dot = dot;
	}



	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}



	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (a ? 1231 : 1237);
		result = prime * result + (b ? 1231 : 1237);
		result = prime * result + (c ? 1231 : 1237);
		result = prime * result + (d ? 1231 : 1237);
		result = prime * result + (dot ? 1231 : 1237);
		result = prime * result + (e ? 1231 : 1237);
		result = prime * result + (f ? 1231 : 1237);
		result = prime * result + (g ? 1231 : 1237);
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SevenSegment other = (SevenSegment) obj;
		if (a != other.a)
			return false;
		if (b != other.b)
			return false;
		if (c != other.c)
			return false;
		if (d != other.d)
			return false;
		if (dot != other.dot)
			return false;
		if (e != other.e)
			return false;
		if (f != other.f)
			return false;
		if (g != other.g)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	/**
	 * @return the flash
	 */
	public boolean isFlash() {
		return flash;
	}
	/**
	 * @param flash the flash to set
	 */
	public void setFlash(boolean flash) {
		this.flash = flash;
	}
	/**
	 * @param flash the flash to get
	 */
	public boolean getFlash() {
		return this.flash;
	}
	/**
	 * @return the on
	 */
	public Color getOn() {
		return on;
	}
	/**
	 * @param on the on to set
	 */
	public void setOn(Color on) {
		this.on = on;
	}
	/**
	 * @return the off
	 */
	public Color getOff() {
		return off;
	}
	/**
	 * @param off the off to set
	 */
	public void setOff(Color off) {
		this.off = off;
	}

}
