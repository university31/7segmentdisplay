package org.seven.segment;

import java.awt.EventQueue;
import java.awt.Graphics;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class TestingWindow {

	private JFrame frame;
	private final Action value_set = new SwingAction_1();
	private SevenSegment sevenSegment; 
	private final Action color_set = new SwingAction_2();
	private JTextField txtValue;
	private JTextField txtFlash;
	private final Action flash_start = new SwingAction_3();
	private JComboBox comboColor;
	private final Action delete_disp = new SwingAction();
	private final Action add_disp = new SwingAction_4();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestingWindow window = new TestingWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TestingWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 665, 446);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		sevenSegment = new SevenSegment();
		sevenSegment.setBounds(10, 0, 639, 153);
		frame.getContentPane().add(sevenSegment);
		
		JButton btnNewButton_1 = new JButton("light Up");
		btnNewButton_1.setAction(value_set);
		btnNewButton_1.setBounds(203, 190, 140, 23);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton = new JButton("Stop it");
		btnNewButton.setAction(color_set);
		btnNewButton.setBounds(203, 258, 140, 23);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblPodajWartos = new JLabel("Podaj wartosć:");
		lblPodajWartos.setBounds(108, 159, 85, 23);
		frame.getContentPane().add(lblPodajWartos);
		
		txtValue = new JTextField();
		txtValue.setBounds(203, 159, 140, 20);
		frame.getContentPane().add(txtValue);
		txtValue.setColumns(10);
		
		comboColor = new JComboBox();
		comboColor.setModel(new DefaultComboBoxModel(new String[] {"Czerwony", "Zielony", "Niebieski"}));
		comboColor.setBounds(203, 224, 140, 23);
		frame.getContentPane().add(comboColor);
		
		JLabel lblWybierzKolor = new JLabel("Wybierz kolor:");
		lblWybierzKolor.setBounds(108, 224, 85, 23);
		frame.getContentPane().add(lblWybierzKolor);
		
		JLabel lblPodajIleRazy = new JLabel("Podaj ile razy wyswietlacz ma migać:");
		lblPodajIleRazy.setBounds(10, 293, 193, 23);
		frame.getContentPane().add(lblPodajIleRazy);
		
		txtFlash = new JTextField();
		txtFlash.setBounds(203, 294, 140, 20);
		frame.getContentPane().add(txtFlash);
		txtFlash.setColumns(10);
		
		JButton btnMiganie = new JButton("Miganie");
		btnMiganie.setAction(flash_start);
		btnMiganie.setBounds(203, 327, 140, 23);
		frame.getContentPane().add(btnMiganie);
		
		JButton btnDodajWyswietlacz = new JButton("Dodaj wyswietlacz");
		btnDodajWyswietlacz.setAction(add_disp);
		btnDodajWyswietlacz.setBounds(203, 361, 140, 23);
		frame.getContentPane().add(btnDodajWyswietlacz);
		
		JButton btnUsuWywietlacz = new JButton("Usuń wyświetlacz");
		btnUsuWywietlacz.setAction(delete_disp);
		btnUsuWywietlacz.setBounds(72, 361, 121, 23);
		frame.getContentPane().add(btnUsuWywietlacz);
	}
	private class SwingAction_1 extends AbstractAction {
		public SwingAction_1() {
			putValue(NAME, "Ustaw wartość");
			putValue(SHORT_DESCRIPTION, "Potwierdza wpisaną wartość i wyświetla ja na wyświetlaczu");
		}
		public void actionPerformed(ActionEvent e) {
			sevenSegment.setDisplayValue(txtValue.getText());
		}
	}
	private class SwingAction_2 extends AbstractAction {
		public SwingAction_2() {
			putValue(NAME, "Ustaw kolor");
			putValue(SHORT_DESCRIPTION, "Ustawia kolor wybrany w rozwojanej liście");
		}
		public void actionPerformed(ActionEvent e) {
			sevenSegment.changeSegmentsColor(comboColor.getSelectedIndex() + 1);
		}
	}
	private class SwingAction_3 extends AbstractAction {
		public SwingAction_3() {
			putValue(NAME, "Aktywuj migotanie");
			putValue(SHORT_DESCRIPTION, "Wyświetlacz miga wybraną ilość razy");
		}
		public void actionPerformed(ActionEvent e) {
			sevenSegment.flashing(Integer.parseInt(txtFlash.getText()), txtValue.getText());
		}
	}
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "Usun wyswietlacz");
			putValue(SHORT_DESCRIPTION, "Usuwa wyświetlacz");
		}
		public void actionPerformed(ActionEvent e) {
			sevenSegment.deleteDisplay(sevenSegment.getGraphics());
		}
	}
	private class SwingAction_4 extends AbstractAction {
		public SwingAction_4() {
			putValue(NAME, "Dodaj wyswietlacz");
			putValue(SHORT_DESCRIPTION, "Dodaje kolejny wyświetlacz");
		}
		public void actionPerformed(ActionEvent e) {
			sevenSegment.addDisplay(sevenSegment.getGraphics());
		}
	}
}
